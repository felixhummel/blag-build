# Usage
In a [Sphinx](https://www.sphinx-doc.org/) directory:
```
docker run --rm \
     --volume "$PWD:/usr/src/blag" \
     --user $(id -u):$(id -g) \
     registry.gitlab.com/felixhummel/blag-build:master \
     sphinx-build -b html -d _build/doctrees . _build/html
```

HTML should be available in `_build/html`.


# Live-Reload
While writing locally:
```
docker run --rm \
     --volume "$PWD:/usr/src/blag" \
     --user $(id -u):$(id -g) \
     registry.gitlab.com/felixhummel/blag-build:master \
     ./reloader.py
```


# Development
```
make
```

When done, wait for CI and pull again
```
git push
python -m http.browser https://gitlab.com/felixhummel/blag-build/pipelines
# ...
docker pull registry.gitlab.com/felixhummel/blag-build:master
```


# How to Update
```
pip install -U pur
pur
```
